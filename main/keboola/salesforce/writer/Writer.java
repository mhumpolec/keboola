package keboola.salesforce.writer;

import java.io.*;
import java.util.*;

import com.sforce.async.*;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

import keboola.salesforce.writer.config.JsonConfigParser;
import keboola.salesforce.writer.config.KBCConfig;

/**
 *
 * @author David Esner <esnerda at gmail.com>
 * @author Martin Humpolec <martin.humpolec at gmail.com>
 * @created 2016
 */
public class Writer {
//
	public static int nrErrors = 0;
	
	public static void main(String[] args) throws AsyncApiException, ConnectionException, IOException   {
		if (args.length == 0) {
			System.err.println("No parameters provided.");
			System.exit(1);
		}

		String dataPath = args[0];
		String inTablesPath = dataPath + File.separator + "in" + File.separator + "tables" + File.separator;
		
		KBCConfig config = null;
		File confFile = new File(args[0] + File.separator + "config.json");
		if (!confFile.exists()) {
			System.out.println("config.json does not exist!");
			System.err.println("config.json does not exist!");
			System.exit(1);
		}
		// Parse config file
		try {
			if (confFile.exists() && !confFile.isDirectory()) {
				config = JsonConfigParser.parseFile(confFile);
			}
		} catch (Exception ex) {
			System.out.println("Failed to parse config file");
			System.err.println(ex.getMessage());
			System.exit(1);
		}
		if (!config.validate()) {
			System.out.println(config.getValidationError());
			System.err.println(config.getValidationError());
			System.exit(1);
		}

		System.out.println( "Update 2020/07/22 - added ID to error message (if possible).");
		System.out.println( "Update 2020/04/20 - add more details about error when logging to Salesforce.");
		System.out.println( "Update 2019/03/07 - ability to link to other records via external ID - see the replaceString settings.");
		
   		System.out.println( "Everything ready, write to Salesforce, loginname: " + config.getParams().getLoginname() + ", operation: " + config.getParams().getOperation() + ", serial mode: " + config.getParams().getSerialMode());
		Writer sfupd = new Writer();
		sfupd.runUpdate(config.getParams().getLoginname(),
				config.getParams().getPassword() + config.getParams().getSecuritytoken(), inTablesPath, config.getParams().getSandbox(), config.getParams().getOperation(), config.getParams().getUpsertField().trim(), config.getParams().getAssignmentId().trim(), config.getParams().getSerialMode(), config.getParams().getReplaceString());
				
   		System.out.println( "All done");
   		if( nrErrors > 0){
   			System.err.println( "There were some errors.");
   			System.exit(1);
   		}
	}


/**
	 * Creates a Bulk API job and uploads batches for a CSV file.
	 */
	public void runUpdate(String userName, String password, String filesDirectory, boolean sandbox, String operation, String upsertField, String assignmentId, boolean serialMode, String replaceString)
			throws AsyncApiException, ConnectionException, IOException {
		BulkConnection connection = getBulkConnection(userName, password, sandbox);
		
		File folder = new File( filesDirectory);
		File[] listOfFiles = folder.listFiles();

    	for (int i = 0; i < listOfFiles.length; i++) {
      	if (listOfFiles[i].isFile()) {
			 String fileName = listOfFiles[i].getName().toString();
			 int position = fileName.indexOf('.');
			 String fileNameShort = fileName.substring( 0, position);
			 boolean manifest = fileName.endsWith( "manifest");
			 if( manifest == false) {
				System.out.println( "found file " + fileName + ", object " + fileNameShort);
				JobInfo job = createJob( fileNameShort, connection, operation, upsertField, assignmentId, serialMode);
				List<BatchInfo> batchInfoList = createBatchesFromCSVFile(connection, job, filesDirectory + listOfFiles[i].getName(), replaceString);
				closeJob(connection, job.getId());
				awaitCompletion(connection, job, batchInfoList);
				checkResults(connection, job, batchInfoList);
			}
      	  }
    	}
	}

	/**
	 * Gets the results of the operation and checks for errors.
	 */
	private void checkResults(BulkConnection connection, JobInfo job, List<BatchInfo> batchInfoList)
			throws AsyncApiException, IOException {
		// batchInfoList was populated when batches were created and submitted
		
		long nrOfUpdates = 0;
		long nrOfCreated = 0;
		
		for (BatchInfo b : batchInfoList) {
			try {
				CSVReader rdr = new CSVReader(connection.getBatchResultStream(job.getId(), b.getId()));
				List<String> resultHeader = rdr.nextRecord();
				int resultCols = resultHeader.size();

				List<String> row;
				while ((row = rdr.nextRecord()) != null) {
					Map<String, String> resultInfo = new HashMap<String, String>();
					for (int i = 0; i < resultCols; i++) {
						resultInfo.put(resultHeader.get(i), row.get(i));
					}
					boolean success = Boolean.valueOf(resultInfo.get("Success"));
					boolean created = Boolean.valueOf(resultInfo.get("Created"));
					String id = resultInfo.get("Id");
					String error = resultInfo.get("Error");

					if (success && created) {
						nrOfCreated++;
						// System.out.println("Updated row with id " + id);
					} else if( success && !created){
						nrOfUpdates++;
					} else if (!success) {
						if( nrErrors < 50) {
							System.out.println("Failed with error: " + error + ", id: " + id);
						} else if( nrErrors == 50)  {
							System.out.println( "Skipping future errors...");
						}
						nrErrors++;
					}
				}
			} catch ( Exception e) {
				System.err.println( "checkResult error " + e.getMessage() );
				nrErrors++;
			}
		}
		System.out.println( "Created " + String.valueOf( nrOfCreated) + " records.");
		System.out.println( "Updated " + String.valueOf( nrOfUpdates) + " records.");
	}

	private void closeJob(BulkConnection connection, String jobId) throws AsyncApiException {
		JobInfo job = new JobInfo();
		job.setId(jobId);
		job.setState(JobStateEnum.Closed);
		connection.updateJob(job);
	}

	/**
	 * Wait for a job to complete by polling the Bulk API.
	 * 
	 * @param connection
	 *            BulkConnection used to check results.
	 * @param job
	 *            The job awaiting completion.
	 * @param batchInfoList
	 *            List of batches for this job.
	 * @throws AsyncApiException
	 */
	private void awaitCompletion(BulkConnection connection, JobInfo job, List<BatchInfo> batchInfoList)
			throws AsyncApiException {
		long sleepTime = 0L;
		Set<String> incomplete = new HashSet<String>();
		for (BatchInfo bi : batchInfoList) {
			incomplete.add(bi.getId());
		}
		while (!incomplete.isEmpty()) {
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
			}
			System.out.println("Awaiting results..." + incomplete.size());
			sleepTime = 10000L;
			try {
				BatchInfo[] statusList = connection.getBatchInfoList(job.getId()).getBatchInfo();
				for (BatchInfo b : statusList) {
					if (b.getState() == BatchStateEnum.Completed || b.getState() == BatchStateEnum.Failed) {
						if( b.getStateMessage() != null) { System.err.println("BATCH STATUS MESSAGE: " + b.getStateMessage());}
						if (incomplete.remove(b.getId())) {
// debug only							System.out.println("BATCH STATUS:\n" + b);
						}
					}
				}
			} catch ( Exception e) {
				System.err.println( "awaitCompletion error " + e.getMessage() );
				nrErrors++;
			}
		}
	}

	/**
	 * Create a new job using the Bulk API.
	 * 
	 * @param sobjectType
	 *            The object type being loaded, such as "Account"
	 * @param connection
	 *            BulkConnection used to create the new job.
	 * @return The JobInfo for the new job.
	 * @throws AsyncApiException
	 */
	private JobInfo createJob(String sobjectType, BulkConnection connection, String operation, String upsertField, String assignmentId, boolean serialMode) throws AsyncApiException {
		try {
			System.out.println( "Starting job");
			JobInfo job = new JobInfo();
			job.setObject(sobjectType);
			switch( operation){
				case "insert": job.setOperation(OperationEnum.insert);
					break;
				case "upsert": job.setOperation(OperationEnum.upsert);
					job.setExternalIdFieldName( upsertField);
					break;
				case "delete": job.setOperation(OperationEnum.delete);
					break;
				default: job.setOperation(OperationEnum.update);
			}
			job.setContentType(ContentType.CSV);
			if( serialMode==true) { 
				System.out.println( "Setting serial mode");
				job.setConcurrencyMode(ConcurrencyMode.Serial);
			}
			
			if( assignmentId != null && !assignmentId.isEmpty()) {
				job.setAssignmentRuleId( assignmentId);
			}
			job = connection.createJob(job);
// debug only			System.out.println(job);
			return job;
		}
		catch ( Exception e) {
			System.err.println( "createJob error " + e.getMessage() );
			nrErrors++;
			return null;
		}
	}

	/**
	 * Create the BulkConnection used to call Bulk API operations.
	 */
	private BulkConnection getBulkConnection(String userName, String password, boolean sandbox)
			throws ConnectionException, AsyncApiException {
		try {
			ConnectorConfig partnerConfig = new ConnectorConfig();
			partnerConfig.setUsername(userName);
			partnerConfig.setPassword(password);
			if ( sandbox == true)  {
				System.out.println("Connecting to Salesforce Sandbox");
				partnerConfig.setAuthEndpoint("https://test.salesforce.com/services/Soap/u/43.0");
			} else {
				System.out.println("Connecting to Salesforce Production");
				partnerConfig.setAuthEndpoint("https://login.salesforce.com/services/Soap/u/43.0");			
			}
			// Creating the connection automatically handles login and stores
			// the session in partnerConfig
			new PartnerConnection(partnerConfig);
			// When PartnerConnection is instantiated, a login is implicitly
			// executed and, if successful,
			// a valid session is stored in the ConnectorConfig instance.
			// Use this key to initialize a BulkConnection:
			ConnectorConfig config = new ConnectorConfig();
			config.setSessionId(partnerConfig.getSessionId());
			// The endpoint for the Bulk API service is the same as for the normal
			// SOAP uri until the /Soap/ part. From here it's '/async/versionNumber'
			String soapEndpoint = partnerConfig.getServiceEndpoint();
			String apiVersion = "43.0";
			String restEndpoint = soapEndpoint.substring(0, soapEndpoint.indexOf("Soap/")) + "async/" + apiVersion;
			config.setRestEndpoint(restEndpoint);
			// This should only be false when doing debugging.
			config.setCompression(true);
			// Set this to true to see HTTP requests and responses on stdout
			config.setTraceMessage(false);
			BulkConnection connection = new BulkConnection(config);
			return connection;
		} catch( Exception ex){
			System.err.println( "Error logging into the system - " + ex.getMessage());
			System.err.println( "If you changed password don't forget to change security token as well.");
			System.exit(1);
			return null;
		}
	}

	/**
	 * Create and upload batches using a CSV file. The file into the appropriate
	 * size batch files.
	 * 
	 * @param connection
	 *            Connection to use for creating batches
	 * @param jobInfo
	 *            Job associated with new batches
	 * @param csvFileName
	 *            The source file for batch data
	 */
	private List<BatchInfo> createBatchesFromCSVFile(BulkConnection connection, JobInfo jobInfo, String csvFileName, String replaceString)
			throws IOException, AsyncApiException {
		List<BatchInfo> batchInfos = new ArrayList<BatchInfo>();
		BufferedReader rdr = new BufferedReader(new InputStreamReader(new FileInputStream(csvFileName)));
		// read the CSV header row, replace char if needed for dot
		byte[] headerBytes;
		if( replaceString != ""){
			headerBytes = (rdr.readLine().replace( replaceString, ".") + "\n").getBytes("UTF-8");
		} else {
			headerBytes = (rdr.readLine() + "\n").getBytes("UTF-8");
		}
		int headerBytesLength = headerBytes.length;
		File tmpFile = File.createTempFile("bulkAPIUpdate", ".csv");

		// Split the CSV file into multiple batches
		try {
			FileOutputStream tmpOut = new FileOutputStream(tmpFile);
			int maxBytesPerBatch = 10000000; // 10 million bytes per batch
			int maxRowsPerBatch = 10000; // 10 thousand rows per batch
			int currentBytes = 0;
			int currentLines = 0;
			String nextLine;
			while ((nextLine = rdr.readLine()) != null) {
				byte[] bytes = (nextLine + "\n").getBytes("UTF-8");
				// Create a new batch when our batch size limit is reached
				if (currentBytes + bytes.length > maxBytesPerBatch || currentLines > maxRowsPerBatch) {
					createBatch(tmpOut, tmpFile, batchInfos, connection, jobInfo);
					currentBytes = 0;
					currentLines = 0;
				}
				if (currentBytes == 0) {
					tmpOut = new FileOutputStream(tmpFile);
					tmpOut.write(headerBytes);
					currentBytes = headerBytesLength;
					currentLines = 1;
				}
				tmpOut.write(bytes);
				currentBytes += bytes.length;
				currentLines++;
			}
			// Finished processing all rows
			// Create a final batch for any remaining data
			if (currentLines > 1) {
				createBatch(tmpOut, tmpFile, batchInfos, connection, jobInfo);
			}
		} finally {
			tmpFile.delete();
		}
		return batchInfos;

	}

	/**
	 * Create a batch by uploading the contents of the file. This closes the
	 * output stream.
	 * 
	 * @param tmpOut
	 *            The output stream used to write the CSV data for a single
	 *            batch.
	 * @param tmpFile
	 *            The file associated with the above stream.
	 * @param batchInfos
	 *            The batch info for the newly created batch is added to this
	 *            list.
	 * @param connection
	 *            The BulkConnection used to create the new batch.
	 * @param jobInfo
	 *            The JobInfo associated with the new batch.
	 */
	private void createBatch(FileOutputStream tmpOut, File tmpFile, List<BatchInfo> batchInfos,
			BulkConnection connection, JobInfo jobInfo) throws IOException, AsyncApiException {
		tmpOut.flush();
		tmpOut.close();
		FileInputStream tmpInputStream = new FileInputStream(tmpFile);
		try {
			BatchInfo batchInfo = connection.createBatchFromStream(jobInfo, tmpInputStream);
//	debug only		System.out.println(batchInfo);
			batchInfos.add(batchInfo);

		} finally {
			tmpInputStream.close();
		}
	}

}